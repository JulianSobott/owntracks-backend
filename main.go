package main

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"gorm.io/driver/postgres"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
	"os"
)

var DB *gorm.DB

type Location struct {
	User      string
	Device    string
	Type      string  `json:"_type"`
	Acc       uint    `json:"acc"`
	Alt       float64 `json:"alt"`
	Batt      int     `json:"batt"`
	Bs        int     `json:"bs"`
	Conn      string  `json:"conn"`
	CreatedAt uint64  `json:"created_at"`
	Lat       float64 `json:"lat"`
	Lon       float64 `json:"lon"`
	T         string  `json:"t"`
	Tid       string  `json:"tid"`
	Tst       uint64  `json:"tst"`
	Vac       int     `json:"vac"`
	Vel       int     `json:"vel"`
}

func postLocation(c *gin.Context) {
	var location Location

	if c.ShouldBindJSON(&location) == nil {
		location.Device = c.GetHeader("X-Limit-D")
		location.User = c.GetHeader("X-Limit-U")
		DB.Create(location)
	}

	response := make([]string, 0)
	c.JSON(200, response)
}

func connectSqlite() *gorm.DB {
	db, err := gorm.Open(sqlite.Open("test.db"), &gorm.Config{})
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	return db
}

func connectPostgres() *gorm.DB {
	dsn := fmt.Sprintf("host=%s user=%s password=%s dbname=%s port=5432 sslmode=disable", os.Getenv("DB_HOST"), os.Getenv("DB_USER"), os.Getenv("DB_PASSWORD"), os.Getenv("DB_NAME"))
	db, err := gorm.Open(postgres.Open(dsn), &gorm.Config{})
	if err != nil {
		panic("failed to connect database")
	}
	return db
}

func setupDB() {
	if os.Getenv("DB_TYPE") == "sqlite" {
		DB = connectSqlite()
	} else {
		DB = connectPostgres()
	}
	err := DB.AutoMigrate(&Location{})
	if err != nil {
		panic("Failed to auto migrate" + err.Error())
	}
}

func main() {
	setupDB()
	r := gin.Default()
	r.POST("/api/location", postLocation)
	err := r.Run() // listen and serve on 0.0.0.0:8080
	if err != nil {
		panic(err)
	}
}
