FROM golang:1.18 AS build-env
WORKDIR /src

ADD ./go.mod .
RUN go mod download

ADD . .
RUN go build -o owntracks_backend

WORKDIR /app
RUN cp /src/owntracks_backend .


FROM debian
WORKDIR /app

ENV GIN_MODE=release
ENV PORT=8080
ENV DB_PATH=/data/locations.db

COPY --from=build-env /app .

VOLUME /data
EXPOSE 8080

ENTRYPOINT ["/app/owntracks_backend"]